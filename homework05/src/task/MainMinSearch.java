package task;
import java.util.InputMismatchException;
import java.util.Scanner;

public class MainMinSearch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите числовой массив. Каждое число вводится через Enter, Массив заканчивается на -1.");
        System.out.println();

        try {
            int number = scanner.nextInt();

            int min = number % 10;
            while (number != -1) {
                if (number == 0) {
                    min = 0;
                } else {
                    while (number != 0) {
                        int mod = number % 10;
                        if (mod < min) {
                            min = mod;
                        }
                        number = number / 10;
                    }
                }
                number = scanner.nextInt();
            }
            System.out.println("Минимальное число в массиве: " + min);
        } catch (InputMismatchException e) {
            System.err.println("Введено не числовое значение");
        }

    }
}
